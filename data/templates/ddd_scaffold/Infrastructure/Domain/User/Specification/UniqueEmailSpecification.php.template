<?php

namespace %ddd_blueprint_namespace%\Infrastructure\Domain\User\Specification;

use Doctrine\ORM\NonUniqueResultException;
use %ddd_blueprint_namespace%\Domain\Shared\Specification\AbstractSpecification;
use %ddd_blueprint_namespace%\Domain\User\Exception\EmailAlreadyExistsException;
use %ddd_blueprint_namespace%\Domain\User\Repository\UserRepositoryInterface;
use %ddd_blueprint_namespace%\Domain\User\Specification\UniqueEmailSpecificationInterface;
use %ddd_blueprint_namespace%\Domain\User\ValueObject\Email;

final class UniqueEmailSpecification extends AbstractSpecification implements UniqueEmailSpecificationInterface
{
    private UserRepositoryInterface $userRepository;

    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @param Email $email
     * @return bool
     * @throws EmailAlreadyExistsException
     */
    public function isUnique(Email $email): bool
    {
        return $this->isSatisfiedBy((string)$email);
    }

    /**
     * @param mixed $email
     * @return bool
     * @throws EmailAlreadyExistsException
     */
    public function isSatisfiedBy($email): bool
    {
        try {
            if ($this->userRepository->findByEmail($email)) {
                throw new EmailAlreadyExistsException();
            }
        } catch (NonUniqueResultException $e) {
            throw new EmailAlreadyExistsException();
        }

        return true;
    }
}
