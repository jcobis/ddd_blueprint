<?php

namespace %ddd_blueprint_namespace%\Domain\Shared\Helper;

trait TranslatableContentTrait
{
    /**
     * @param string $attribute
     * @param string|null $language
     * @return mixed
     */
    public function translate(string $attribute, ?string $language = null)
    {
        return $this->getTranslation($attribute, $language);
    }

    /**
     * @param string $attribute
     * @param string|null $language
     * @return mixed
     */
    public function getTranslation(string $attribute, ?string $language = null)
    {
        $translations = $this->getTranslations($attribute);

        return !$language ? $this->getTranslations($attribute) : ($translations[$this->getLanguage($language)] ?? null);
    }

    /**
     * @param string|null $attribute
     * @return array
     */
    public function getTranslations(?string $attribute = null): array
    {
        if (!$attribute) {
            $translations = [];
            foreach ($this->translatableAttributes ?? [] as $translatableAttribute) {
                $translations[$translatableAttribute] = $this->getTranslations($translatableAttribute);
            }
            return $translations;
        }

        return $this->{$attribute} ?? [];
    }

    /**
     * @param string $attribute
     * @param string|null $language
     * @return bool
     */
    public function hasTranslation(string $attribute, ?string $language = null): bool
    {
        return isset($this->{$attribute}[$this->getLanguage($language)]);
    }

    /**
     * @param string $attribute
     * @param string $language
     * @param mixed $translation
     * @return $this
     */
    public function setTranslation(string $attribute, string $language, $translation): self
    {
        $translations = $this->getTranslations($attribute);
        $translations[$language] = $translation;
        $this->{$attribute} = $translations;

        return $this;
    }

    /**
     * @param string $attribute
     * @param array $translations
     * @return $this
     */
    public function setTranslations(string $attribute, array $translations): self
    {
        foreach ($translations as $language => $translation) {
            $this->setTranslation($attribute, $language, $translation);
        }

        return $this;
    }

    /**
     * @param $language
     * @return string|null
     */
    protected function getLanguage(?string $language = null): string
    {
        return $language ?: locale_get_default();
    }
}