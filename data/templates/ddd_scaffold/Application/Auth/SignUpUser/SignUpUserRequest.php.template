<?php

namespace %ddd_blueprint_namespace%\Application\Auth\SignUpUser;

use Assert\AssertionFailedException;
use %ddd_blueprint_namespace%\Application\Shared\BaseRequest;
use %ddd_blueprint_namespace%\Domain\Shared\Exception\ValidationException;
use %ddd_blueprint_namespace%\Domain\Shared\Helper\Assert;
use %ddd_blueprint_namespace%\Domain\User\ValueObject\Password;

class SignUpUserRequest extends BaseRequest
{
    /**
     * @var mixed
     */
    protected $email;

    /**
     * @var mixed
     */
    protected $password;

    /**
     * @var mixed
     */
    protected $passwordConfirmation;

    /**
     * @param mixed $email
     * @param mixed $password
     * @param mixed $passwordConfirmation
     * @throws AssertionFailedException
     * @throws ValidationException
     */
    public function __construct($email, $password, $passwordConfirmation)
    {
        $this->setEmail($email);
        $this->setPassword($password, $passwordConfirmation);
    }

    /**
     * @param mixed $email
     * @throws AssertionFailedException
     */
    private function setEmail($email): void
    {
        $propertyPath = 'email';

        Assert::notBlank($email, null, $propertyPath);
        Assert::email($email, null, $propertyPath);

        $this->email = $email;
    }

    /**
     * @param mixed $password
     * @param mixed $passwordConfirmation
     * @throws AssertionFailedException
     * @throws ValidationException
     */
    private function setPassword($password, $passwordConfirmation): void
    {
        $propertyPath = 'password';
        $confirmationPropertyPath = 'password_confirmation';

        Assert::notBlank($password, null, $propertyPath);
        Assert::minLength($password, Password::MIN_PASSWORD_LENGTH, null, $propertyPath);
        Assert::propertyConfirmationMatch(
            $password,
            $passwordConfirmation,
            null,
            $propertyPath,
            $confirmationPropertyPath
        );

        $this->password = $password;
    }
}
