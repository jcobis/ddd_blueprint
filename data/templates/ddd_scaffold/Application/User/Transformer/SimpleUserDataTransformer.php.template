<?php

namespace %ddd_blueprint_namespace%\Application\User\Transformer;

use %ddd_blueprint_namespace%\Application\Shared\DataTransformerInterface;
use %ddd_blueprint_namespace%\Domain\User\User;

class SimpleUserDataTransformer implements DataTransformerInterface
{
    /**
     * @param User $user
     * @param User|null $sessionUser
     * @param bool $onlyToBeConsumedByBackOffice
     * @return array
     */
    public function transform($user, User $sessionUser = null, bool $onlyToBeConsumedByBackOffice = false): array
    {
        $userData = $user->getData();

        return [
            'id' => $userData['id'],
            'email' => $userData['email'],
            'name' => $userData['name'],
            'surname' => $userData['surname'],
            'created_at' => $userData['created_at']->getTimestamp(),
            'updated_at' => $userData['updated_at']->getTimestamp(),
        ];
    }
}
