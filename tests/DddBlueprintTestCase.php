<?php

namespace App\Tests;

/*
 * This file is part of the DDD-Blueprint package.
 *
 * (c) Jorge Cobis <jcobis@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

use PHPUnit\Framework\TestCase;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;

class DddBlueprintTestCase extends TestCase
{
    /**
     * Status code when everything went fine
     */
    public const NORMAL_EXIT_CODE_ON_RUNNING_COMMAND = 0;

    /**
     * @var array
     */
    protected static $tempFolders = [];

    /**
     * @var false|string
     */
    protected $tempFolder;


    protected function setUp(): void
    {
        $tempFolder = sprintf("%s/%f.%d", sys_get_temp_dir(), microtime(true), mt_rand());
        (new Filesystem)->mkdir($tempFolder);

        $this->tempFolder = realpath($tempFolder);
        self::$tempFolders[] = $this->tempFolder;
    }

    public static function tearDownAfterClass(): void
    {
//        (new Filesystem)->remove(self::$tempFolders);
    }

    /**
     * @param string $originPath
     * @param string $targetPath
     * @param array $exclusions
     */
    public function assertMirror(string $originPath, string $targetPath, array $exclusions = []): void
    {
        $finder = new Finder;
        $baseLine = array_map(
            static function (string $path) use ($originPath) {
                return str_replace($originPath . '/', null, $path);
            },
            array_keys(iterator_to_array($finder->in($originPath)))
        );

        $finder = new Finder;
        $build = array_map(
            static function (string $path) use ($targetPath) {
                return str_replace($targetPath . '/', null, $path);
            },
            array_keys(iterator_to_array($finder->in($targetPath)))
        );

        $array_diff = array_diff($build, $baseLine, $exclusions);

        $this->assertEquals(
            [],
            $array_diff,
            sprintf("\"%s\" is not a mirror of \"%s\"", $targetPath, $originPath)
        );
    }
}
