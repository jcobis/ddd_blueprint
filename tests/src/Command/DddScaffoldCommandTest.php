<?php

/*
 * This file is part of the DDD-Blueprint package.
 *
 * (c) Jorge Cobis <jcobis@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Tests\Command;

use App\Command\DddInitCommand;
use App\Command\DddScaffoldCommand;
use App\Tests\DddBlueprintTestCase;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Tester\CommandTester;

class DddScaffoldCommandTest extends DddBlueprintTestCase
{
    /**
     * @var CommandTester
     */
    private $dddScaffoldCommandTester;

    /**
     * @var CommandTester
     */
    private $dddInitCommandTester;


    protected function setUp(): void
    {
        parent::setUp();

        $application = new Application();
        $application->add(new DddScaffoldCommand);
        $application->add(new DddInitCommand);

        $this->dddInitCommandTester = new CommandTester(
            $application->find('ddd:init')
        );

        $this->dddScaffoldCommandTester = new CommandTester(
            $application->find('ddd:scaffold')
        );
    }

    /**
     * @test
     */
    public function it_should_runs_successfully(): void
    {
        $projectFolder = $this->executeDddInitCommand();

        $this->dddScaffoldCommandTester->execute(
            [
                'project-folder' => $projectFolder
            ]
        );

        $statusCode = $this->dddScaffoldCommandTester->getStatusCode();

        self::assertEquals(0, $statusCode);
    }

    /**
     * @return string
     */
    private function executeDddInitCommand(): string
    {
        $inputs = ['test-ddd.org', 'test_ddd', 'TestDDD', 'TestDDD', 'database-image'];

        $this->dddInitCommandTester->setInputs($inputs);
        $this->dddInitCommandTester->execute(
            [
                'build-folder' => $this->tempFolder
            ]
        );

        return sprintf("%s/%s", $this->tempFolder, $inputs[0]);
    }
}
