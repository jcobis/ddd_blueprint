<?php

namespace App\Tests\Command;

use App\Command\DddInitCommand;
use App\Tests\DddBlueprintTestCase;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Tester\CommandTester;

class DddInitCommandTest extends DddBlueprintTestCase
{
    /**
     * @var CommandTester
     */
    private $dddInitCommandTester;


    protected function setUp(): void
    {
        parent::setUp();

        $application = new Application();
        $application->add(new DddInitCommand);
        $command = $application->find('ddd:init');

        $this->dddInitCommandTester = new CommandTester($command);
    }

    /**
     * @test
     * @dataProvider missingMandatoryArguments
     * @param array $inputs
     */
    public function it_should_returns_a_non_zero_status_code_when_any_of_mandatory_arguments_is_missing(array $inputs
    ): void {
        $this->dddInitCommandTester->setInputs($inputs);
        $this->dddInitCommandTester->execute([]);

        self::assertNotEquals(
            self::NORMAL_EXIT_CODE_ON_RUNNING_COMMAND,
            $this->dddInitCommandTester->getStatusCode()
        );
    }

    /**
     * @test
     */
    public function it_should_runs_successfully(): void
    {
        $inputs = ['test-ddd.org', 'test_ddd', 'TestDDD', 'TestDDD'];

        $this->dddInitCommandTester->setInputs($inputs);
        $this->dddInitCommandTester->execute(
            [
                'build-folder' => $this->tempFolder
            ]
        );

        $statusCode = $this->dddInitCommandTester->getStatusCode();

        self::assertEquals(0, $statusCode);
    }

    /**
     * @return array
     */
    public function missingMandatoryArguments(): array
    {
        $emptyMainFolder = (string)null;
        $emptyDockerBaseName = (string)null;
        $emptySrcFolder = (string)null;
        $emptyAppNamespace = (string)null;

        return [
            [[$emptyMainFolder]],
            [['ddd.org', $emptyDockerBaseName]],
            [['ddd.org', 'ddd', $emptySrcFolder]],
            [['ddd.org', 'ddd', 'DDD', $emptyAppNamespace]]
        ];
    }
}
