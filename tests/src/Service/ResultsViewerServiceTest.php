<?php

/*
 * This file is part of the DDD-Blueprint package.
 *
 * (c) Jorge Cobis <jcobis@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Tests\Service;

use App\Service\ResultsViewerService;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Console\Style\SymfonyStyle;

class ResultsViewerServiceTest extends TestCase
{
    /**
     * @test
     */
    public function it_should_outputs_a_formatted_text(): void
    {
        $io = $this->createMock(SymfonyStyle::class);

        $viewerService = new ResultsViewerService($io);

        self::assertTrue(
            $viewerService->execute(
                'Ok',
                [
                    'section#1' => [],
                    'section#2' => ['result']
                ]
            )
        );
    }
}
