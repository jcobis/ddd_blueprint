<?php

/*
 * This file is part of the DDD-Blueprint package.
 *
 * (c) Jorge Cobis <jcobis@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Tests\Service;

use App\Service\SymfonyFolderRearrangeService;
use App\Tests\DddBlueprintTestCase;
use Symfony\Component\Filesystem\Filesystem;

class SymfonyFolderRearrangerServiceTest extends DddBlueprintTestCase
{
    private const DDD_MAIN_FOLDER = 'ddd_test';

    /**
     * @var Filesystem
     */
    private $filesystem;


    protected function setUp(): void
    {
        parent::setUp();

        $this->filesystem = new Filesystem;
    }

    /**
     * @test
     */
    public function it_should_removes_unnecessary_symfony_folders(): void
    {
        $configFile = sprintf("data/%s", SymfonyFolderRearrangeService::FOLDERS_FOR_REMOVING_CONFIG_FILE);

        $foldersToBeRemoved = json_decode(
            file_get_contents($configFile),
            true
        );

        $createdFolders = $this->createFolderRequiredByTest($foldersToBeRemoved);

        $symfonyFolderRearrangeService = new SymfonyFolderRearrangeService($this->filesystem);
        $removedFolders = $symfonyFolderRearrangeService->execute(self::DDD_MAIN_FOLDER, $this->tempFolder);

        self::assertTrue(
            $this->areDeletedAllCreatedFolders($createdFolders)
        );

        self::assertEquals($createdFolders, $removedFolders);
    }

    /**
     * @param $foldersToBeRemoved
     * @return array
     */
    private function createFolderRequiredByTest($foldersToBeRemoved): array
    {
        $createdFolders = [];
        foreach ($foldersToBeRemoved as $path => $folders) {
            array_map(
                function (string $folder) use ($path, &$createdFolders) {
                    $folderToBeRemoved = sprintf(
                        "%s/%s/%s/%s",
                        $this->tempFolder,
                        self::DDD_MAIN_FOLDER,
                        $path,
                        $folder
                    );

                    $this->filesystem->mkdir($folderToBeRemoved);
                    $createdFolders[] = str_replace('build/', null, $folderToBeRemoved);

                    return $folderToBeRemoved;
                },
                $folders
            );
        }
        return $createdFolders;
    }

    /**
     * @param array $createdFolders
     * @return bool
     */
    private function areDeletedAllCreatedFolders(array $createdFolders): bool
    {
        foreach ($createdFolders as $folder) {
            if ($this->filesystem->exists($folder)) {
                return false;
            }
        }

        return true;
    }
}
