<?php

/*
 * This file is part of the DDD-Blueprint package.
 *
 * (c) Jorge Cobis <jcobis@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Tests\Service;

use App\Exception\DddConfigFileCorruptedException;
use App\Exception\DddConfigFileNotFoundException;
use App\Exception\MainFolderNotFoundException;
use App\Service\ScaffoldDataLoaderService;
use App\Tests\DddBlueprintTestCase;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;

class ScaffoldDataLoaderServiceTest extends DddBlueprintTestCase
{
    private const DDD_MAIN_FOLDER = 'ddd_test';

    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * @var Finder
     */
    private $finder;


    protected function setUp(): void
    {
        parent::setUp();

        $this->filesystem = new Filesystem;
        $this->finder = new Finder;
    }

    /**
     * @test
     * @throws DddConfigFileCorruptedException
     * @throws DddConfigFileNotFoundException
     * @throws MainFolderNotFoundException
     */
    public function it_should_throws_a_main_folder_not_found_exception_when_the_folder_to_be_scaffolding_is_missing(
    ): void
    {
        $this->expectException(MainFolderNotFoundException::class);

        $scaffoldDataLoaderService = new ScaffoldDataLoaderService(
            $this->filesystem,
            $this->finder
        );

        $scaffoldDataLoaderService->execute(
            self::DDD_MAIN_FOLDER,
            $this->tempFolder
        );
    }

    /**
     * @test
     * @throws DddConfigFileCorruptedException
     * @throws DddConfigFileNotFoundException
     * @throws MainFolderNotFoundException
     */
    public function it_should_throws_a_ddd_config_file_not_found_exception_when_config_file_is_missing(): void
    {
        $this->expectException(DddConfigFileNotFoundException::class);

        $this->filesystem->mkdir(
            sprintf("%s/%s", $this->tempFolder, self::DDD_MAIN_FOLDER)
        );

        $scaffoldDataLoaderService = new ScaffoldDataLoaderService(
            $this->filesystem,
            $this->finder
        );

        $scaffoldDataLoaderService->execute(
            self::DDD_MAIN_FOLDER,
            $this->tempFolder
        );
    }

    /**
     * @test
     * @throws DddConfigFileCorruptedException
     * @throws DddConfigFileNotFoundException
     * @throws MainFolderNotFoundException
     */
    public function it_should_throws_a_ddd_config_file_corrupted_exception_when_the_content_of_config_file_is_not_the_expected_one(
    ): void
    {
        $this->expectException(DddConfigFileCorruptedException::class);

        $dddProjectFolder = sprintf("%s/%s", $this->tempFolder, self::DDD_MAIN_FOLDER);

        $configData = [
            'main-folder' => 'ddd-blueprint.test',
            'docker-base-name' => '',
            'src-folder' => null,
            'app-namespace' => 'DBT'
        ];

        $this->filesystem->mkdir($dddProjectFolder);

        file_put_contents(
            sprintf("%s/%s", $dddProjectFolder, ScaffoldDataLoaderService::BASE_VALUES_FILE),
            json_encode($configData)
        );

        $scaffoldDataLoaderService = new ScaffoldDataLoaderService(
            $this->filesystem,
            $this->finder
        );

        $scaffoldDataLoaderService->execute(
            self::DDD_MAIN_FOLDER,
            $this->tempFolder
        );
    }

    /**
     * @test
     * @throws DddConfigFileCorruptedException
     * @throws DddConfigFileNotFoundException
     * @throws MainFolderNotFoundException
     */
    public function it_should_load_expected_values_of_config_file(): void
    {
        $dddProjectFolder = sprintf("%s/%s", $this->tempFolder, self::DDD_MAIN_FOLDER);

        $configData = [
            'main-folder' => 'ddd-blueprint.test',
            'docker-base-name' => 'ddd_blueprint_test',
            'src-folder' => 'DddBlueprintTest',
            'app-namespace' => 'DBT'
        ];

        $this->filesystem->mkdir($dddProjectFolder);

        file_put_contents(
            sprintf("%s/%s", $dddProjectFolder, ScaffoldDataLoaderService::BASE_VALUES_FILE),
            json_encode($configData)
        );

        $scaffoldDataLoaderService = new ScaffoldDataLoaderService(
            $this->filesystem,
            $this->finder
        );

        $loadedConfigValues = $scaffoldDataLoaderService->execute(
            self::DDD_MAIN_FOLDER,
            $this->tempFolder
        );

        self::assertEquals($configData, $loadedConfigValues);
    }
}
