<?php

/*
 * This file is part of the DDD-Blueprint package.
 *
 * (c) Jorge Cobis <jcobis@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Tests\Service;

use App\Exception\TemplateNotFoundException;
use App\Service\TemplateProcessingService;
use App\Tests\DddBlueprintTestCase;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;

class TemplateProcessingServiceTest extends DddBlueprintTestCase
{
    private const TEMPLATE_VALUES = ['tv1', 'tv2', 'tv3', 'tv4'];
    private const TARGET_FOLDER = 'test-ddd-templates';

    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * @var Finder
     */
    private $finder;


    protected function setUp(): void
    {
        parent::setUp();

        $this->filesystem = new Filesystem;
        $this->finder = new Finder;

        $this->filesystem->mkdir(
            sprintf("%s/%s", $this->tempFolder, self::TARGET_FOLDER)
        );
    }

    /**
     * @test
     * @throws TemplateNotFoundException
     */
    public function it_should_throws_a_template_not_found_exception_when_template_base_files_are_missing(): void
    {
        $this->expectException(TemplateNotFoundException::class);

        $templateProcessingService = new TemplateProcessingService(
            $this->filesystem,
            $this->finder,
            self::TARGET_FOLDER,
            $this->tempFolder
        );

        $templateProcessingService->execute(self::TEMPLATE_VALUES);
    }

    /**
     * @test
     * @throws TemplateNotFoundException
     */
    public function it_should_processes_a_template_file(): void
    {
        $templateFileName = sprintf(
            "%s/%s/%s%s",
            $this->tempFolder,
            self::TARGET_FOLDER,
            'test',
            TemplateProcessingService::TEMPLATE_FILE_EXTENSION
        );

        $processedTemplateFileName = str_replace(
            TemplateProcessingService::TEMPLATE_FILE_EXTENSION,
            null,
            $templateFileName
        );

        file_put_contents($templateFileName, TemplateProcessingService::DDD_BLUEPRINT_PLACEHOLDERS);

        $templateProcessingService = new TemplateProcessingService(
            $this->filesystem,
            $this->finder,
            self::TARGET_FOLDER,
            $this->tempFolder
        );

        $processedTemplates = $templateProcessingService->execute(self::TEMPLATE_VALUES);

        $processedTemplateContent = file_get_contents($processedTemplateFileName);

        $regex = sprintf("/(%s)/", implode('|', TemplateProcessingService::DDD_BLUEPRINT_PLACEHOLDERS));
        self::assertNotRegExp($regex, $processedTemplateContent);
        self::assertEquals(implode('', self::TEMPLATE_VALUES), $processedTemplateContent);

        self::assertFalse(
            $this->filesystem->exists($templateFileName)
        );
        self::assertTrue(
            $this->filesystem->exists($processedTemplateFileName)
        );

        self::assertCount(1, $processedTemplates);
        self::assertEquals($processedTemplateFileName, $processedTemplates[0]);
    }

    /**
     * @test
     * @dataProvider actionsFixtures
     * @param string $takenAction
     * @throws TemplateNotFoundException
     */
    public function it_should_add_info_about_the_taken_action_after_template_was_processed(string $takenAction): void
    {
        $templateFileName = sprintf(
            "%s/%s/%s%s",
            $this->tempFolder,
            self::TARGET_FOLDER,
            'test',
            TemplateProcessingService::TEMPLATE_FILE_EXTENSION
        );

        file_put_contents($templateFileName, TemplateProcessingService::DDD_BLUEPRINT_PLACEHOLDERS);

        $processedTemplateFileName = str_replace(
            TemplateProcessingService::TEMPLATE_FILE_EXTENSION,
            null,
            $templateFileName
        );

        if (TemplateProcessingService::OVERWRITTEN_ACTION === $takenAction) {
            file_put_contents($processedTemplateFileName, null);
        }

        $templateProcessingService = new TemplateProcessingService(
            $this->filesystem,
            $this->finder,
            self::TARGET_FOLDER,
            $this->tempFolder
        );

        $processedTemplates = $templateProcessingService->execute(self::TEMPLATE_VALUES, true);

        $regex = sprintf("/\(%s\)$/", $takenAction[0]);
        self::assertRegExp(
            $regex,
            $processedTemplates[0]
        );
    }

    /**
     * @return array
     */
    public function actionsFixtures(): array
    {
        return [
            [TemplateProcessingService::CREATED_ACTION],
            [TemplateProcessingService::OVERWRITTEN_ACTION]
        ];
    }
}
