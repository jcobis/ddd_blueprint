<?php

namespace App\Tests\Service;

use App\Exception\MainFolderAlreadyExistsException;
use App\Exception\MainFolderCreationException;
use App\Service\BaseLineBuilderService;
use App\Tests\DddBlueprintTestCase;
use Symfony\Component\Filesystem\Exception\IOException;
use Symfony\Component\Filesystem\Filesystem;

class BaseLineBuilderServiceTest extends DddBlueprintTestCase
{
    /**
     * @var string
     */
    private $buildFolder;

    /**
     * @var array
     */
    private $inputs;

    /**
     * @var Filesystem
     */
    private $filesystem;


    protected function setUp(): void
    {
        parent::setUp();

        $this->filesystem = new Filesystem;
        $this->buildFolder = $this->tempFolder; // Just for semantic reasons.

        $this->inputs = [
            'main-folder' => 'ddd-blueprint.test',
            'docker-base-name' => 'ddd_blueprint_test',
            'src-folder' => 'DddBlueprintTest',
            'app-namespace' => 'DBT',
            'database-image' => 'database-image-test'
        ];
    }

    /**
     * @test
     * @throws MainFolderAlreadyExistsException
     * @throws MainFolderCreationException
     */
    public function it_should_throws_a_main_folder_already_exists_exception(): void
    {
        $this->expectException(MainFolderAlreadyExistsException::class);

        $this->filesystem->mkdir(
            sprintf("%s/%s", $this->buildFolder, $this->inputs['main-folder'])
        );

        $baseLineBuilderService = new BaseLineBuilderService(
            $this->filesystem,
            $this->inputs['main-folder'],
            $this->inputs['src-folder'],
            $this->buildFolder
        );

        $baseLineBuilderService->execute($this->inputs);
    }

    /**
     * @test
     * @throws MainFolderAlreadyExistsException
     * @throws MainFolderCreationException
     */
    public function it_should_throws_a_main_folder_creation_exception_when_main_folder_cannot_be_created(): void
    {
        $this->expectException(MainFolderCreationException::class);

        $mockFilesystem = $this->createMock(Filesystem::class);
        $mockFilesystem
            ->method('mkdir')
            ->willThrowException(new IOException('IOException'));

        $baseLineBuilderService = new BaseLineBuilderService(
            $mockFilesystem,
            $this->inputs['main-folder'],
            $this->inputs['src-folder'],
            $this->buildFolder
        );

        $baseLineBuilderService->execute($this->inputs);
    }

    /**
     * @test
     * @throws MainFolderAlreadyExistsException
     * @throws MainFolderCreationException
     */
    public function it_should_rename_the_baseline_folder_after_mirror_it(): void
    {
        $baseLineBuilderService = new BaseLineBuilderService(
            $this->filesystem,
            $this->inputs['main-folder'],
            $this->inputs['src-folder'],
            $this->buildFolder
        );

        $baseLineBuilderService->execute($this->inputs);

        $renamedBaselineFolder = sprintf(
            "%s/%s/www/%s/%s",
            $this->buildFolder,
            $this->inputs['main-folder'],
            BaseLineBuilderService::DEFAULT_SRC_FOLDER,
            $this->inputs['src-folder']
        );

        self::assertTrue(
            $this->filesystem->exists($renamedBaselineFolder)
        );
    }

    /**
     * @test
     * @throws MainFolderAlreadyExistsException
     * @throws MainFolderCreationException
     */
    public function it_should_build_the_baseline_for_a_ddd_project(): void
    {
        $baseLineBuilderService = new BaseLineBuilderService(
            $this->filesystem,
            $this->inputs['main-folder'],
            $this->inputs['src-folder'],
            $this->buildFolder
        );

        $baseLineBuilderService->execute($this->inputs);

        $originPath = BaseLineBuilderService::TEMPLATES_BASELINE_FOLDER;
        $targetPath = sprintf("%s/%s", $this->buildFolder, $this->inputs['main-folder']);
        $exclusions = ['ddd_base_values.json', 'www/src/DddBlueprintTest'];

        $this->assertMirror($originPath, $targetPath, $exclusions);
    }

    /**
     * @test
     * @throws MainFolderAlreadyExistsException
     * @throws MainFolderCreationException
     */
    public function it_should_exists_base_value_data_file_after_mirror_the_baseline_folder()
    {
        $baseLineBuilderService = new BaseLineBuilderService(
            $this->filesystem,
            $this->inputs['main-folder'],
            $this->inputs['src-folder'],
            $this->buildFolder
        );

        $baseLineBuilderService->execute($this->inputs);

        $baseValueDataFile = sprintf(
            "%s/%s/%s",
            $this->buildFolder,
            $this->inputs['main-folder'],
            BaseLineBuilderService::BASE_VALUES_FILE
        );

        self::assertTrue(
            $this->filesystem->exists($baseValueDataFile)
        );

        return $baseValueDataFile;
    }

    /**
     * @test
     * @depends it_should_exists_base_value_data_file_after_mirror_the_baseline_folder
     * @param string $baseValueDataFile
     */
    public function it_should_verifies_that_the_base_values_data_file_not_be_corrupted(string $baseValueDataFile): void
    {
        $fileContent = json_decode(
            file_get_contents($baseValueDataFile),
            true
        );

        self::assertEquals($this->inputs, $fileContent);
    }
}
