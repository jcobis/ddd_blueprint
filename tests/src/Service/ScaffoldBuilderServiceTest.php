<?php

/*
 * This file is part of the DDD-Blueprint package.
 *
 * (c) Jorge Cobis <jcobis@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Tests\Service;

use App\Exception\TemplateNotFoundException;
use App\Service\ScaffoldBuilderService;
use App\Service\ScaffoldDataLoaderService;
use App\Service\TemplateProcessingService;
use App\Tests\DddBlueprintTestCase;
use Symfony\Component\Filesystem\Filesystem;

class ScaffoldBuilderServiceTest extends DddBlueprintTestCase
{
    /**
     * @var TemplateProcessingService
     */
    private $templateProcessingService;

    /**
     * @var Filesystem
     */
    private $filesystem;


    protected function setUp(): void
    {
        $this->filesystem = new Filesystem;

        $scaffoldBaseFolders = [
            ScaffoldBuilderService::TEMPLATES_FOLDER_FOR_SYMFONY_PACKAGES_CONFIG,
            ScaffoldBuilderService::TEMPLATES_FOLDER_FOR_DDD_SCAFFOLD
        ];

        if (!$this->filesystem->exists($scaffoldBaseFolders)) {
            self::markTestSkipped('Missing main scaffold folders!');
        }

        $this->templateProcessingService = $this->createMock(TemplateProcessingService::class);

        parent::setUp();
    }

    /**
     * @test
     * @throws TemplateNotFoundException
     */
    public function it_should_performs_an_scaffolding_on_ddd_project_folder(): void
    {
        $configData = [
            'main-folder' => 'ddd-blueprint.test',
            'docker-base-name' => 'ddd_blueprint_test',
            'src-folder' => 'DddBlueprintTest',
            'app-namespace' => 'DBT'
        ];

        $dddProjectFolder = sprintf(
            "%s/%s",
            $this->tempFolder,
            $configData['main-folder']
        );

        $configFolder = sprintf("%s/www/config", $dddProjectFolder);
        $srcFolder = sprintf("%s/www/src/%s", $dddProjectFolder, $configData['src-folder']);
        $baseValuesFile = sprintf("%s/%s", $dddProjectFolder, ScaffoldDataLoaderService::BASE_VALUES_FILE);

        $this->filesystem->mkdir($dddProjectFolder);
        file_put_contents($baseValuesFile, null);

        (new ScaffoldBuilderService($this->filesystem, $this->templateProcessingService))
            ->execute($configData, $this->tempFolder);

        $this->assertMirror(
            ScaffoldBuilderService::TEMPLATES_FOLDER_FOR_SYMFONY_PACKAGES_CONFIG,
            $configFolder
        );

        $this->assertMirror(
            ScaffoldBuilderService::TEMPLATES_FOLDER_FOR_DDD_SCAFFOLD,
            $srcFolder
        );

        self::assertFalse(
            $this->filesystem->exists($baseValuesFile)
        );
    }
}
