<?php

use App\Command\DddInitCommand;
use App\Command\DddScaffoldCommand;
use Symfony\Component\Console\Application;

require 'vendor/autoload.php';

$application = new Application('DDD console utility', 'v0.2');

$application->add(new DddInitCommand());
$application->add(new DddScaffoldCommand());

try {
    $application->run();
} catch (Exception $e) {
    return -1;
}
