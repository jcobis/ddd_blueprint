<?php

namespace App\Command;

use App\Exception\DddConfigFileCorruptedException;
use App\Exception\DddConfigFileNotFoundException;
use App\Exception\MainFolderNotFoundException;
use App\Exception\TemplateNotFoundException;
use App\Service\ResultsViewerService;
use App\Service\ScaffoldBuilderService;
use App\Service\ScaffoldDataLoaderService;
use App\Service\SymfonyFolderRearrangeService;
use App\Service\TemplateProcessingService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;

class DddScaffoldCommand extends Command
{
    public const DEFAULT_BUILD_FOLDER = 'build';

    /**
     * @var string
     */
    protected static $defaultName = 'ddd:scaffold';


    protected function configure(): void
    {
        $description =
            'Generates a DDD folder structure by rearranging the folders of the Symfony framework installed.';

        $this->setDescription($description);
        $this->addArgument('project-folder', InputArgument::REQUIRED, 'DDD project folder name');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $filesystem = new Filesystem;
        $finder = new Finder;

        $io = new SymfonyStyle($input, $output);

        $mainFolderPathInfo = pathinfo(
            $input->getArgument('project-folder')
        );

        $buildFolder = '.' === $mainFolderPathInfo['dirname'] ?
            self::DEFAULT_BUILD_FOLDER : $mainFolderPathInfo['dirname'];

        $mainProjectFolder = $mainFolderPathInfo['basename'];

        try {
            $dddBaseValues = (new ScaffoldDataLoaderService($filesystem, $finder))
                ->execute($mainProjectFolder, $buildFolder);

            $removedFolders = (new SymfonyFolderRearrangeService($filesystem))
                ->execute($dddBaseValues['main-folder'], $buildFolder);

            $templateProcessingService = new TemplateProcessingService(
                $filesystem,
                $finder,
                sprintf("%s/www", $mainProjectFolder),
                $buildFolder
            );

            $dddScaffoldBuild = (new ScaffoldBuilderService($filesystem, $templateProcessingService))
                ->execute($dddBaseValues, $buildFolder);

            (new ResultsViewerService($io))->execute(
                sprintf("DDD scaffolding successfully applied on \"%s/%s\" folder", $buildFolder, $mainProjectFolder),
                [
                    'Symfony\'s folders deleted' => $removedFolders,
                    'DDD build process overview (c=created, o=overridden)' => $dddScaffoldBuild,
                    'Final steps' => array_filter(
                        [
                            'Review all created/overwrite config files (pay special attention on ' .
                            '"config/services.yaml", "config/packages/doctrine.yaml" and "config/packages/doctrine_migrations.yaml").',
                            'Review the "www/.env.local" file to double check it fits your needs for your local develop environment.',
                            'Double check Symfony framework is up and running properly.',
                            'For Linux users, make executable the script "develop.sh" (chmod +x develop.sh), for using ' .
                            'shortcuts commands on Docker and Symfony framework'
                        ]
                    )
                ]
            );

            return 0;
        } catch (
        DddConfigFileCorruptedException
        | DddConfigFileNotFoundException
        | MainFolderNotFoundException
        | TemplateNotFoundException $e
        ) {
            $io->error($e->getMessage());
            return -1;
        }
    }
}
