<?php

namespace App\Command;

use App\Exception\MainFolderAlreadyExistsException;
use App\Exception\MainFolderCreationException;
use App\Exception\MissingArgumentException;
use App\Exception\TemplateNotFoundException;
use App\Service\BaseLineBuilderService;
use App\Service\ResultsViewerService;
use App\Service\TemplateProcessingService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;

class DddInitCommand extends Command
{
    public const DEFAULT_BUILD_FOLDER = 'build';

    public const MYSQL_IMAGE = 'mysql';
    public const POSTGRES_IMAGE = 'postgres';

    /**
     * @var string
     */
    protected static $defaultName = 'ddd:init';


    protected function configure(): void
    {
        $description =
            'Generates the basic folder structure for creating all "Docker" containers required by the application.';

        $this->setDescription($description);

        $this
            ->addArgument(
                'build-folder',
                InputArgument::OPTIONAL,
                'Folder where DDD baseline will be built',
                self::DEFAULT_BUILD_FOLDER
            )
            ->addOption(
                'main-folder',
                'm',
                InputOption::VALUE_REQUIRED,
                'Main folder\'s name'
            )
            ->addOption(
                'docker-base-name',
                'd',
                InputOption::VALUE_REQUIRED,
                'Base name for docker containers'
            )
            ->addOption(
                'src-folder',
                's',
                InputOption::VALUE_REQUIRED,
                'Source folder name (will be under \'src/\')'
            )
            ->addOption(
                'app-namespace',
                'x',
                InputOption::VALUE_REQUIRED,
                'Application namespace'
            )
            ->addOption(
                'database-image',
                'i',
                InputOption::VALUE_OPTIONAL,
                'Database docker image'
            );
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        try {
            $buildFolder = $input->getArgument('build-folder');

            if (!$dddBlueprintMainFolder = $input->getOption('main-folder')) {
                $dddBlueprintMainFolder = $io->ask('What will be the name for the project\'s folder?');
                if (!$dddBlueprintMainFolder) {
                    throw new MissingArgumentException('Project folder\'s name is mandatory!');
                }
            }

            if (!$dddBlueprintDockerBaseName = $input->getOption('docker-base-name')) {
                $dddBlueprintDockerBaseName = $io->ask('Docker base name for containers?');
                if (!$dddBlueprintDockerBaseName) {
                    throw new MissingArgumentException('Docker container\'s base name is mandatory!');
                }
            }

            if (!$dddBlueprintSrcFolder = $input->getOption('src-folder')) {
                $dddBlueprintSrcFolder = $io->ask('Source folder name (will be under \'src/\')?');
                if (!$dddBlueprintSrcFolder) {
                    throw new MissingArgumentException('Source folder name is mandatory!');
                }
            }

            if (!$dddBlueprintNamespace = $input->getOption('app-namespace')) {
                $dddBlueprintNamespace = $io->ask('Namespace?');
                if (!$dddBlueprintNamespace) {
                    throw new MissingArgumentException('Namespace is mandatory!');
                }
            }

            if (!$dddBlueprintDataBaseImage = $input->getOption('database-image')) {
                $questionHelper = $this->getHelper('question');
                $question = new ChoiceQuestion(
                    sprintf('Select the docker database image will be used (default "%s")', self::MYSQL_IMAGE),
                    [self::MYSQL_IMAGE, self::POSTGRES_IMAGE],
                    self::MYSQL_IMAGE
                );
                $question->setErrorMessage('Database image %s is invalid.');

                $dddBlueprintDataBaseImage = $questionHelper->ask($input, $output, $question);
//
//                if (!$dddBlueprintNamespace) {
//                    throw new MissingArgumentException('Namespace is mandatory!');
//                }
            }
//            die($dddBlueprintDataBaseImage);


            $inputs = [
                'main-folder' => $dddBlueprintMainFolder,
                'docker-base-name' => $dddBlueprintDockerBaseName,
                'src-folder' => $dddBlueprintSrcFolder,
                'app-namespace' => $dddBlueprintNamespace,
                'database-image' => $dddBlueprintDataBaseImage
            ];

            $filesystem = new Filesystem;
            $finder = new Finder;

            (new BaseLineBuilderService($filesystem, $dddBlueprintMainFolder, $dddBlueprintSrcFolder, $buildFolder))
                ->execute($inputs);

            $baseFiles = (new TemplateProcessingService($filesystem, $finder, $dddBlueprintMainFolder, $buildFolder))
                ->execute($inputs);

            $io = new SymfonyStyle($input, $output);
            (new ResultsViewerService($io))->execute(
                sprintf("DDD Project's container was created in \"%s/%s\"", $buildFolder, $dddBlueprintMainFolder),
                [
                    'Generated files' => $baseFiles,
                    'Next steps' => [
                        'Review all docker config files. Modify them according your needs.',
                        'Build docker containers (usually this can be done by executing "docker-compose up" command).',
                        'Double check docker containers are up and running properly.',
                        'Install Symfony framework (by running "composer install" on docker\'s PHP container).',
                        'Double check Symfony framework is up an running properly.',
                        'Run "php ddd_console.php ddd:scaffold ' . $dddBlueprintMainFolder . '" for getting rearranged ' .
                        'the Symfony project into one with a DDD approach.',
                        'For Linux users, make executable the script "develop.sh" (chmod +x develop.sh), for using shortcuts commands on Docker'
                    ]
                ]
            );

            return 0;
        } catch (
        MissingArgumentException
        | MainFolderAlreadyExistsException
        | TemplateNotFoundException
        | MainFolderCreationException $e
        ) {
            $io->error($e->getMessage());
            return -1;
        }
    }
}
