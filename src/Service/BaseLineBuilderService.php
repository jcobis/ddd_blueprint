<?php

namespace App\Service;

use App\Exception\MainFolderAlreadyExistsException;
use App\Exception\MainFolderCreationException;
use JsonException;
use Symfony\Component\Filesystem\Exception\IOException;
use Symfony\Component\Filesystem\Filesystem;

class BaseLineBuilderService
{
    public const TEMPLATES_BASELINE_FOLDER = 'data/templates/baseline';
    public const DEFAULT_SRC_FOLDER = 'src';
    public const DDD_ROOT_FOLDER = 'ddd_blueprint';
    public const BASE_VALUES_FILE = 'ddd_base_values.json';

    /**
     * @var string
     */
    private $dddBlueprintMainFolder;

    /**
     * @var string
     */
    private $dddBlueprintSrcFolder;

    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * @var string
     */
    private $buildFolder;

    /**
     * @param Filesystem $filesystem
     * @param string $mainFolder
     * @param string $srcFolder
     * @param string $buildFolder
     */
    public function __construct(
        Filesystem $filesystem,
        string $mainFolder,
        string $srcFolder,
        string $buildFolder = 'build'
    ) {
        $this->filesystem = $filesystem;
        $this->dddBlueprintMainFolder = $mainFolder;
        $this->dddBlueprintSrcFolder = $srcFolder;
        $this->buildFolder = $buildFolder;
    }

    /**
     * @param array $inputs
     * @throws MainFolderAlreadyExistsException
     * @throws MainFolderCreationException
     * @throws JsonException
     */
    public function execute(array $inputs): void
    {
        $buildFolder = sprintf("%s/%s", $this->buildFolder, $this->dddBlueprintMainFolder);

        if ($this->filesystem->exists($buildFolder)) {
            throw new MainFolderAlreadyExistsException('Folder\'s name already exists!');
        }

        try {
            $this->filesystem->mkdir($buildFolder);
        } catch (IOException $exception) {
            throw new MainFolderCreationException($exception->getMessage());
        }

        $this->filesystem->mirror(self::TEMPLATES_BASELINE_FOLDER, $buildFolder);

        $dddBasePath = sprintf("%s/www/%s/", $buildFolder, self::DEFAULT_SRC_FOLDER);

        $this->filesystem->rename(
            $dddBasePath . self::DDD_ROOT_FOLDER,
            $dddBasePath . $this->dddBlueprintSrcFolder
        );

        file_put_contents(
            sprintf("%s/%s", $buildFolder, self::BASE_VALUES_FILE),
            json_encode($inputs, JSON_PRETTY_PRINT | JSON_THROW_ON_ERROR)
        );
    }
}
