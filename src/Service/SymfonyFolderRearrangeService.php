<?php

/*
 * This file is part of the DDD-Blueprint package.
 *
 * (c) Jorge Cobis <jcobis@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Service;

use Symfony\Component\Filesystem\Filesystem;

class SymfonyFolderRearrangeService
{
    public const FOLDERS_FOR_REMOVING_CONFIG_FILE = 'folders-for-removing.json';
    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * @param Filesystem $filesystem
     */
    public function __construct(Filesystem $filesystem)
    {
        $this->filesystem = $filesystem;
    }

    /**
     * @param string $dddBlueprintMainFolder
     * @param string $buildFolder
     * @return array
     */
    public function execute(string $dddBlueprintMainFolder, string $buildFolder = 'build'): array
    {
        $removedFolders = [];
        $configFile = sprintf("data/%s", self::FOLDERS_FOR_REMOVING_CONFIG_FILE);

        $foldersToBeRemoved = json_decode(
            file_get_contents($configFile),
            true
        );

        foreach ($foldersToBeRemoved ?? [] as $path => $folders) {
            $this->filesystem->remove(
                array_map(
                    function (string $folder) use ($buildFolder, $dddBlueprintMainFolder, $path, &$removedFolders) {
                        $folderToBeRemoved = sprintf(
                            "%s/%s/%s/%s",
                            $buildFolder,
                            $dddBlueprintMainFolder,
                            $path,
                            $folder
                        );

                        if ($this->filesystem->exists($folderToBeRemoved)) {
                            $removedFolders[] = str_replace('build/', null, $folderToBeRemoved);
                        }

                        return $folderToBeRemoved;
                    },
                    $folders
                )
            );
        }

        return $removedFolders;
    }
}
