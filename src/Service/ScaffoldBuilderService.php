<?php

/*
 * This file is part of the DDD-Blueprint package.
 *
 * (c) Jorge Cobis <jcobis@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Service;

use App\Exception\TemplateNotFoundException;
use Symfony\Component\Filesystem\Filesystem;

class ScaffoldBuilderService
{
    public const TEMPLATES_FOLDER_FOR_SYMFONY_PACKAGES_CONFIG = 'data/templates/overrides/config';
    public const TEMPLATES_FOLDER_FOR_DDD_SCAFFOLD = 'data/templates/ddd_scaffold';

    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * @var TemplateProcessingService
     */
    private $templateProcessingService;

    /**
     * @param Filesystem $filesystem
     * @param TemplateProcessingService $templateProcessingService
     */
    public function __construct(Filesystem $filesystem, TemplateProcessingService $templateProcessingService)
    {
        $this->filesystem = $filesystem;
        $this->templateProcessingService = $templateProcessingService;
    }

    /**
     * @param array $dddBaseValues
     * @param string $buildFolder
     * @return array
     * @throws TemplateNotFoundException
     */
    public function execute(array $dddBaseValues, string $buildFolder = 'build'): array
    {
        $configFolder = sprintf(
            "%s/%s/www/config",
            $buildFolder,
            $dddBaseValues['main-folder']
        );

        $srcFolder = sprintf(
            "%s/%s/www/src/%s",
            $buildFolder,
            $dddBaseValues['main-folder'],
            $dddBaseValues['src-folder']
        );

        $this->filesystem->mirror(self::TEMPLATES_FOLDER_FOR_SYMFONY_PACKAGES_CONFIG, $configFolder);
        $this->filesystem->mirror(self::TEMPLATES_FOLDER_FOR_DDD_SCAFFOLD, $srcFolder);

        $templatesProcessingResult = $this->templateProcessingService->execute($dddBaseValues, true);

        $this->filesystem->remove(
            sprintf(
                "%s/%s/%s",
                $buildFolder,
                $dddBaseValues['main-folder'],
                ScaffoldDataLoaderService::BASE_VALUES_FILE
            )
        );

        return $templatesProcessingResult;
    }
}
