<?php

namespace App\Service;

use App\Exception\TemplateNotFoundException;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;

class TemplateProcessingService
{
    public const TEMPLATE_FILE_EXTENSION = '.template';

    public const CREATED_ACTION = 'created';
    public const OVERWRITTEN_ACTION = 'overwritten';

    public const DDD_BLUEPRINT_MAIN_FOLDER_PLACEHOLDER = '%ddd_blueprint_main_folder%';
    public const DDD_BLUEPRINT_DOCKER_BASE_NAME_PLACEHOLDER = '%ddd_blueprint_docker_base_name%';
    public const DDD_BLUEPRINT_SRC_FOLDER_PLACEHOLDER = '%ddd_blueprint_src_folder%';
    public const DDD_BLUEPRINT_NAMESPACE_PLACEHOLDER = '%ddd_blueprint_namespace%';
    public const DDD_BLUEPRINT_DATABASE_IMAGE_PLACEHOLDER = '%__include_database_image__%';
    public const DDD_BLUEPRINT_DATABASE_IMAGE_OVERRIDE_LINUX_PLACEHOLDER = '%__include_database_override_linux__%';
    public const DDD_BLUEPRINT_DATABASE_PORT_OVERRIDE_PLACEHOLDER = '%__include_database_port_override_linux__%';

    public const DDD_BLUEPRINT_PLACEHOLDERS = [
        self::DDD_BLUEPRINT_MAIN_FOLDER_PLACEHOLDER,
        self::DDD_BLUEPRINT_DOCKER_BASE_NAME_PLACEHOLDER,
        self::DDD_BLUEPRINT_SRC_FOLDER_PLACEHOLDER,
        self::DDD_BLUEPRINT_NAMESPACE_PLACEHOLDER
    ];

    public const DDD_BLUEPRINT_INCLUDE_PLACEHOLDERS = [
        self::DDD_BLUEPRINT_DATABASE_IMAGE_PLACEHOLDER,
        self::DDD_BLUEPRINT_DATABASE_IMAGE_OVERRIDE_LINUX_PLACEHOLDER,
        self::DDD_BLUEPRINT_DATABASE_PORT_OVERRIDE_PLACEHOLDER
    ];

    /**
     * @var Filesystem
     */
    private Filesystem $filesystem;

    /**
     * @var Finder
     */
    private Finder $finder;

    /**
     * @var string
     */
    private string $dddMainFolder;

    /**
     * @param Filesystem $filesystem
     * @param Finder $finder
     * @param string $targetFolder
     * @param string $buildFolder
     */
    public function __construct(
        Filesystem $filesystem,
        Finder $finder,
        string $targetFolder,
        string $buildFolder = 'build'
    ) {
        $this->filesystem = $filesystem;
        $this->finder = $finder;
        $this->dddMainFolder = sprintf("%s/%s", $buildFolder, $targetFolder);
    }

    /**
     * @param array $templateValues
     * @param bool $showActionTaken
     * @return array
     * @throws TemplateNotFoundException
     */
    public function execute(array $templateValues, $showActionTaken = false): array
    {
        $processedTemplates = [];

        $this->finder
            ->in($this->dddMainFolder)
            ->exclude($this->dddMainFolder . '/vendor')
            ->files()
            ->notName(ScaffoldDataLoaderService::BASE_VALUES_FILE)
            ->ignoreDotFiles(false)
            ->name(sprintf("/\\%s$/", self::TEMPLATE_FILE_EXTENSION));

        if (!$this->finder->hasResults()) {
            throw new TemplateNotFoundException(
                sprintf("Template files not found in % folder!", $this->dddMainFolder)
            );
        }

        foreach ($this->finder as $file) {
            $absoluteFilePath = $file->getRealPath();
            $templateContent = $file->getContents();

            foreach (self::DDD_BLUEPRINT_INCLUDE_PLACEHOLDERS as $includePlaceholder) {
                if (strpos($templateContent, $includePlaceholder)) {

                    $includeFileName = str_replace(
                        ['%__include_', '__%', '_'],
                        [$templateValues['database-image'] . '_', '.template', '-'],
                        $includePlaceholder
                    );

                    $templateContent = str_replace(
                        $includePlaceholder,
                        file_get_contents(
                            sprintf("%s/database-images/%s", $this->dddMainFolder, $includeFileName)
                        ),
                        $templateContent
                    );
                }
            }

            $processedContent = str_replace(
                self::DDD_BLUEPRINT_PLACEHOLDERS,
                array_values($templateValues),
                $templateContent
            );

            $processedFileName = str_replace(self::TEMPLATE_FILE_EXTENSION, null, $absoluteFilePath);

            $actionTaken = $this->filesystem
                ->exists($processedFileName) ? self::OVERWRITTEN_ACTION : self::CREATED_ACTION;

            file_put_contents($absoluteFilePath, $processedContent);

            $processedFileName = preg_replace('/.+build\//', null, $processedFileName);

            if ($showActionTaken) {
                $processedFileName = sprintf("%s (%.1s)", $processedFileName, $actionTaken);
            }

            $processedTemplates[] = $processedFileName;
        }

        $this->cleanAndRenameTemplates();

        return $processedTemplates;
    }

    private function cleanAndRenameTemplates(): void
    {
        $this->filesystem->remove(
            sprintf('%s/database-images', $this->dddMainFolder)
        );

        $this->finder
            ->in($this->dddMainFolder)
            ->exclude($this->dddMainFolder . '/vendor')
            ->files()
            ->notName(ScaffoldDataLoaderService::BASE_VALUES_FILE)
            ->ignoreDotFiles(false)
            ->name(sprintf("/\\%s$/", self::TEMPLATE_FILE_EXTENSION));

        foreach ($this->finder as $file) {
            $absoluteFilePath = $file->getRealPath();
            $processedFileName = str_replace(self::TEMPLATE_FILE_EXTENSION, null, $absoluteFilePath);
            $this->filesystem->rename($absoluteFilePath, $processedFileName, true);
        }
    }
}
