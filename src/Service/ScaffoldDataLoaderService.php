<?php

/*
 * This file is part of the DDD-Blueprint package.
 *
 * (c) Jorge Cobis <jcobis@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Service;

use App\Exception\DddConfigFileCorruptedException;
use App\Exception\DddConfigFileNotFoundException;
use App\Exception\MainFolderNotFoundException;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;

class ScaffoldDataLoaderService
{
    public const BASE_VALUES_FILE = 'ddd_base_values.json';

    public const BASE_VALUES_DATA_KEYS = [
        'main-folder',
        'docker-base-name',
        'src-folder',
        'app-namespace'
    ];

    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * @var Finder
     */
    private $finder;

    /**
     * @param Filesystem $filesystem
     * @param Finder $finder
     */
    public function __construct(Filesystem $filesystem, Finder $finder)
    {
        $this->filesystem = $filesystem;
        $this->finder = $finder;
    }

    /**
     * @param string $mainProjectFolder
     * @param string $buildFolder
     * @return array
     * @throws DddConfigFileCorruptedException
     * @throws DddConfigFileNotFoundException
     * @throws MainFolderNotFoundException
     */
    public function execute(string $mainProjectFolder, string $buildFolder = 'build'): array
    {
        $relativeProjectFolder = sprintf("%s/%s", $buildFolder, $mainProjectFolder);

        if (!$this->filesystem->exists($relativeProjectFolder)) {
            throw new MainFolderNotFoundException(
                sprintf("%s not found!", $relativeProjectFolder)
            );
        }

        $this->finder
            ->in($relativeProjectFolder)
            ->files()
            ->name(self::BASE_VALUES_FILE);

        if (!$this->finder->hasResults()) {
            throw new DddConfigFileNotFoundException("DDD config file not found!");
        }

        foreach ($this->finder as $jsonFile) {
            $dddBaseValues = json_decode(
                file_get_contents($jsonFile->getRealPath()),
                true
            );
        }

        $dddBaseValues = array_filter($dddBaseValues ?? []);

        if (0 !== count(array_diff(self::BASE_VALUES_DATA_KEYS, array_keys($dddBaseValues)))) {
            throw new DddConfigFileCorruptedException('DDD config file is corrupted!');
        }

        return $dddBaseValues;
    }
}
