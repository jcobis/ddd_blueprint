<?php

/*
 * This file is part of the DDD-Blueprint package.
 *
 * (c) Jorge Cobis <jcobis@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Service;

use Symfony\Component\Console\Style\SymfonyStyle;

class ResultsViewerService
{
    /**
     * @var SymfonyStyle
     */
    private $io;

    /**
     * @param SymfonyStyle $io
     */
    public function __construct(SymfonyStyle $io)
    {
        $this->io = $io;
    }

    /**
     * @param string $successMessage
     * @param array $results
     * @return bool
     */
    public function execute(string $successMessage, array $results): bool
    {
        $this->io->success($successMessage);

        foreach ($results as $section => $info) {
            if (empty($info)) {
                continue;
            }

            $this->io->section($section);
            $this->io->listing($info);
        }

        $this->io->newLine();
        $this->io->text('Thanks for using DDD Blueprint scaffold! -- :D');
        $this->io->newLine();

        return true;
    }
}